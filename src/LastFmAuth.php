<?php

namespace Drupal\lastfm;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides LastFmAuth, a 'lastfm_auth' service class.
 */
class LastFmAuth implements LastFmAuthInterface {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Static cache of DB select results.
   *
   * @var object
   */
  protected static $cache;

  /**
   * Constructs a new LastFmAuth.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(Connection $database, AccountInterface $current_user) {
    $this->database = $database;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function save($name, $session_key, $subscriber) {
    if ($uid = $this->currentUser->id()) {
      $fields = [
        'name' => $name,
        'session_key' => $session_key,
        'subscriber' => $subscriber,
      ];
      return $this->database->merge('lastfm_auth')
        ->fields($fields)
        ->key('uid', $uid)
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    if (!self::$cache && ($uid = $this->currentUser->id())) {
      self::$cache = $this->database->select('lastfm_auth', 'la')
        ->fields('la')
        ->condition('uid', $uid)
        ->execute()
        ->fetchObject();
    }

    return self::$cache;
  }

}
