<?php

namespace Drupal\lastfm\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\lastfm\LastFmAuthInterface;
use LastFmApi\Api\AuthApi;
use LastFmApi\Api\TrackApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides ScrobbleController class.
 */
class ScrobbleController extends ControllerBase {

  /**
   * The lastfm_auth service.
   *
   * @var \Drupal\lastfm\LastFmAuthInterface
   */
  protected $lastFmAuth;

  /**
   * Constructs a new ScrobbleController.
   *
   * @param \Drupal\lastfm\LastFmAuthInterface $lastfm_auth
   *   The lastfm_auth service.
   */
  public function __construct(LastFmAuthInterface $lastfm_auth) {
    $this->lastFmAuth = $lastfm_auth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('lastfm_auth'));
  }

  /**
   * Checks access for this controller methods.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Allowed if application settings are not empty and user is authenticated
   *   on Last.fm.
   */
  public function access(AccountInterface $account) {
    $current_user = $this->currentUser();
    $id = $this->config('lastfm.settings')->get('id');
    $secret = $this->config('lastfm.settings')->get('secret');
    $session = $this->lastFmAuth->get();

    return AccessResult::allowedIf($account->id() == $current_user->id() && !empty($id) && !empty($secret) && !empty($session));
  }

  /**
   * Setup tasks.
   *
   * These tasks cannot be run in class constructor, because the class is
   * constructed not only in our specific routes, but, for example, just to
   * check access to routes, etc.
   */
  protected function setup() {
    $path_to_module = drupal_get_path('module', 'lastfm');
    $loader = $path_to_module . '/vendor/autoload.php';

    if (file_exists($loader)) {
      // Lib installed in module's vendor dir.
      require_once $loader;
    }
  }

  /**
   * Notifies Last.fm that a user has started listening to a track.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object. Only POST parameters are used.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function updateNowPlaying(Request $request) {
    $post = $request->request->all();
    if (!$post) {
      throw new BadRequestHttpException($this->t('No parameters specified.'));
    }

    $this->setup();

    $auth = new AuthApi('setsession', [
      'apiKey' => $this->config('lastfm.settings')->get('id'),
      'apiSecret' => $this->config('lastfm.settings')->get('secret'),
      'username' => $this->lastFmAuth->get()->name,
      'sessionKey' => $this->lastFmAuth->get()->session_key,
      'subscriber' => $this->lastFmAuth->get()->subscriber,
    ]);
    $track_api = new TrackApi($auth);
    $result = $track_api->updateNowPlaying($post);

    return new JsonResponse($result ? 'OK' : 'Fail');
  }

  /**
   * Adds a track-play to a user's profile.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object. Only POST parameters are used. The 'timestamp' parameter
   *   is added automatically.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function scrobble(Request $request) {
    $post = $request->request->all();
    if (!$post) {
      throw new BadRequestHttpException($this->t('No parameters specified.'));
    }

    $this->setup();

    $auth = new AuthApi('setsession', [
      'apiKey' => $this->config('lastfm.settings')->get('id'),
      'apiSecret' => $this->config('lastfm.settings')->get('secret'),
      'username' => $this->lastFmAuth->get()->name,
      'sessionKey' => $this->lastFmAuth->get()->session_key,
      'subscriber' => $this->lastFmAuth->get()->subscriber,
    ]);
    $track_api = new TrackApi($auth);
    $result = $track_api->scrobble($post);

    return new JsonResponse($result ? 'OK' : 'Fail');
  }

}
