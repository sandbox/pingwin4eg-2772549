<?php

namespace Drupal\lastfm\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\lastfm\LastFmAuthInterface;
use LastFmApi\Api\AuthApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides AuthController class.
 */
class AuthController extends ControllerBase {

  /**
   * The lastfm_auth service.
   *
   * @var \Drupal\lastfm\LastFmAuthInterface
   */
  protected $lastFmAuth;

  /**
   * Constructs a new AuthController.
   *
   * @param \Drupal\lastfm\LastFmAuthInterface $lastfm_auth
   *   The lastfm_auth service.
   */
  public function __construct(LastFmAuthInterface $lastfm_auth) {
    $this->lastFmAuth = $lastfm_auth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('lastfm_auth'));
  }

  /**
   * Checks access for this controller methods based on module's configuration.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Allowed if application settings are not empty.
   */
  public function access(AccountInterface $account) {
    $id = $this->config('lastfm.settings')->get('id');
    $secret = $this->config('lastfm.settings')->get('secret');

    return AccessResult::allowedIf(!empty($id) && !empty($secret));
  }

  /**
   * Setup tasks.
   *
   * These tasks cannot be run in class constructor, because the class is
   * constructed not only in our specific routes, but, for example, just to
   * check access to routes, etc.
   */
  protected function setup() {
    $path_to_module = drupal_get_path('module', 'lastfm');
    $loader = $path_to_module . '/vendor/autoload.php';

    if (file_exists($loader)) {
      // Lib installed in module's vendor dir.
      require_once $loader;
    }
  }

  /**
   * Authenticates current user with their Last.fm account.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Response object.
   */
  public function auth(Request $request) {
    $this->setup();

    $api_key = $this->config('lastfm.settings')->get('id');
    $api_secret = $this->config('lastfm.settings')->get('secret');

    // First step is to get a short time valid token.
    if (!$request->query->get('token')) {
      $callback_url = Url::createFromRequest($request)
        ->setAbsolute()
        // Fool the EarlyRenderingControllerWrapperSubscriber to not throw
        // Exception where not needed.
        ->toString(TRUE)
        ->getGeneratedUrl();

      $url = Url::fromUri('http://www.last.fm/api/auth', [
        'query' => ['api_key' => $api_key, 'cb' => $callback_url],
      ]);

      // Here a user should go to Last.fm and allow access.
      return new TrustedRedirectResponse($url->toString());
    }

    // Now get the actual session data.
    try {
      $auth = new AuthApi('getsession', [
        'apiKey' => $api_key,
        'apiSecret' => $api_secret,
        'token' => $request->query->get('token'),
      ]);

      $result = $this->lastFmAuth->save($auth->username, $auth->sessionKey, $auth->subscriber);

      if ($result) {
        drupal_set_message($this->t('Authenticated successfully.'));
      }
    }
    catch (\Exception $e) {
      watchdog_exception('lastfm', $e);
      drupal_set_message(Html::escape($e->getMessage()), 'error');
    }

    return $this->redirect('<front>');
  }

}
