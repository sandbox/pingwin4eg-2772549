<?php

namespace Drupal\lastfm;

/**
 * Defines an interface for a 'lastfm_auth' service.
 */
interface LastFmAuthInterface {

  /**
   * Saves the Last.fm session information for the current user.
   *
   * @param string $name
   *   Last.fm account name.
   * @param string $session_key
   *   The session key.
   * @param bool $subscriber
   *   Whether user is a paid subscriber.
   *
   * @return int|null
   *   If the record insert or update failed, returns NULL. If it succeeded,
   *   returns one of Merge class constants, depending on the operation
   *   performed.
   */
  public function save($name, $session_key, $subscriber);

  /**
   * Loads the Last.fm session information for the current user.
   *
   * @return object|null
   *   The object with session info if it exists for current user. Otherwise
   *   returns NULL. Fields of returning object:
   *   - uid: ID of the current user.
   *   - name: Last.fm account name.
   *   - session_key: The session key.
   *   - subscriber: Boolean indicating whether user is a paid subscriber.
   */
  public function get();

}
