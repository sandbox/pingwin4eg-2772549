<?php

namespace Drupal\lastfm\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides the Last.fm Auth block.
 *
 * @Block(
 *   id = "lastfm_auth",
 *   admin_label = @Translation("Last.fm Auth"),
 * )
 */
class AuthBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#cache' => [
        'contexts' => ['user.permissions'],
        'tags' => ['config:lastfm.settings'],
      ],
    ];

    $url = Url::fromRoute('lastfm.auth', [], [
      'attributes' => ['rel' => 'nofollow'],
    ]);

    if ($url->access()) {
      $text = $this->t('Get/refresh session key from Last.fm');
      $build['link'] = Link::fromTextAndUrl($text, $url)->toRenderable();
    }

    return $build;
  }

}
